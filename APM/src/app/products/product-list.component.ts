import {Component, OnInit} from '@angular/core';
import {iProduct } from './product';

@Component(
    {
    selector: 'pm-products',
    templateUrl: './product-list.component.html',
    styleUrls: ['./product-list.component.css']
    }
)

export class ProductListComponent  implements OnInit
{
pageTitle: string = 'Product List';
imageWidth: number = 50;
imageMargin: number = 2;
showImage: boolean = true;
filteredProducts: iProduct[];
_listFilter: string;

get listFilter(): string
{
  return this._listFilter;
}
set listFilter(value:string)
{
  this._listFilter = value;
  this.filteredProducts = this.listFilter ? this.performFilter(this.listFilter): this.products;
}



products:  iProduct[]=[ 
    {
        "productId": 2,
        "productName": "Garden Cart",
        "productCode": "GDN-0023",
        "releaseDate": "March 18, 2019",
        "description": "15 gallon capacity rolling garden cart",
        "price": 32.99,
        "starRating": 4.5,
        "imageUrl": "assets/images/garden_cart.png"
    },
    {
        "productId": 5,
        "productName": "Hammer",
        "productCode": "TBX-0048",
        "releaseDate": "May 21, 2019",
        "description": "Curved claw steel hammer",
        "price": 8.9,
        "starRating": 1.5,
        "imageUrl": "assets/images/hammer.png"
    }

];

constructor()
{
 this.filteredProducts = this.products;
 this.listFilter = '';
}

onRatingClicked(message :string): void
{
  this.pageTitle = 'Product LIst: ' + message
}

performFilter(filterBy: string): iProduct[]
{
 filterBy = filterBy.toLocaleLowerCase();

 return this.products.filter((product: iProduct ) => 
    product.productName.toLocaleLowerCase().indexOf(filterBy) !== -1);
}

  toggleImage(): void
  {
    this.showImage = !this.showImage;
  }

  ngOnInit(): void{

    console.log('OnInit');
  }

}