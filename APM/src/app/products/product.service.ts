import {Injectable} from '@angular/core';
import {iProduct} from './product';

@Injectable()
export class ProductService{

    getProducts(): iProduct[]
    {
      return [
       {
        "productId": 2,
        "productName": "Garden Cart",
        "productCode": "GDN-0023",
        "releaseDate": "March 18, 2019",
        "description": "15 gallon capacity rolling garden cart",
        "price": 32.99,
        "starRating": 4.5,
        "imageUrl": "assets/images/garden_cart.png"
    },
    {
        "productId": 5,
        "productName": "Hammer",
        "productCode": "TBX-0048",
        "releaseDate": "May 21, 2019",
        "description": "Curved claw steel hammer",
        "price": 8.9,
        "starRating": 1.5,
        "imageUrl": "assets/images/hammer.png"
    }


]

    }

}